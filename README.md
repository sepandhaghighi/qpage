# QPage
Free Project For Creating  Academic Home page
						                                                                     

1- [Download](https://www.python.org/downloads/) And Install Last Version Of Python ( * or use exe folder and skip this step * )

2- [Download](https://github.com/sepandhaghighi/qpage/archive/v1.5.6.zip) QPage

3- Put your profile picture in image folder (jpg , bmp , tiff , gif , png)

4- Put each page details in a text file with page name in doc folder (* In this step you can use [M] , [S] or [L] at the start and [center] at the end of each line to control font size and alignment )

5- Put your resume file (pdf format) in doc folder

6- Put your font file in font folder (*Optional)

7- Run setup.py

8- Choose your profile image size

9- Choose your text and background color 

10- Finished!!! Now you can go and upload output folder contains directly in your host

Last Version : 1.5.6   

Homepage : [Link](http://sepandhaghighi.github.io/qpage/page.html)

